namespace TodoApiCoreNET.Models
{
    public class TodoItem
    {
        private string _name;
        public string Name {
           get { return _name; }
           set {
               _name = value.Substring(0, 1).ToUpper() + value.Substring(1);
           }
        }
       public long Id {set; get;} 
        
       public bool IsComplete {set; get;} 
       public bool IsImportant {set; get;} 
    }    
}